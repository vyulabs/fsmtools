# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event === EVENTS.%s)' % (capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '(await this.%s())' % machine.input(s.group()).name
    try:
        return 'event === EVENTS.%s' % (capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatAction(a):
    if a.type == 'output':
        return 'await this.%s();' % a.name
    else:
        return 'await this.processEvent(this.Events.%s);' % (machine.name, capitalCase(a.name))

def fromCamelCase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
%>\
%if options.debug:
const moment = require('moment');
const debug = require('debug')('${machine.name.lower()}');
%endif
const _ = require('lodash');
const Promise = require('bluebird');
const assert = require('assert');

const STATES = {
% for (index, s) in enumerate(machine.states):
    % if index == len(machine.states) - 1:
    ${capitalCase(s.name)}: '${fromCamelCase(s.name)}'
    % else:
    ${capitalCase(s.name)}: '${fromCamelCase(s.name)}',
    %endif
% endfor
};

const EVENTS = {
    Init: 0,
% for (index, s) in enumerate(machine.events):
    % if index == len(machine.events) - 1:
    ${capitalCase(s.name)}: ${index + 1}
    % else:
    ${capitalCase(s.name)}: ${index + 1},
    %endif
% endfor
};

function defer() {
    let resolve;
	let reject;
    const promise = new Promise((_resolve, _reject) => {
        resolve = _resolve;
        reject = _reject;
    });
    return {
        resolve: resolve,
        reject: reject,
        promise: promise
    };
}

class FSM {

	constructor(state) {
    	this.state = !state ? STATES.${machine.initialState} : state;
    	this._events = [];
	}

	static states() {
		return STATES;
	}
	
	static events() {
		return EVENTS;
	}
	
	// inputs
% for (index, s) in enumerate(machine.inputs):
	async ${s.name}() { return false; }
% endfor

    // outputs
% for (index, s) in enumerate(machine.outputs):
	async ${s.name}() {}
% endfor

% if options.debug:
	eventName(event) {
    	switch (event) {
        	case 0:
            	return '<Initialization event>';
		% for (index, s) in enumerate(machine.events):
        	case ${index + 1}:
            	return '${s.name}';
    	% endfor
    	}
    	return 'Unknown (' + event + ')';
	}

	stateName(state) {
    	switch (state) {
    	% for (index, s) in enumerate(machine.states):
        	case STATES.${s.name}:
            	return '${s.name}';
    	% endfor
    	}
    	return 'Unknown (' + state + ')';
	}
% endif

	processEventPromise(event) {
    	const empty = _.isEmpty(this._events);

    	const result = defer();
    	this._events.push({event, result});
    	if (empty) {
        	this._processQueue();
        	return result.promise;
    	}
        return Promise.resolve();
	}

	_processQueue() {
    	let self = this;
    	(async () => {
        	while (!_.isEmpty(self._events)) {
            	let x = self._events[0];
            	try {
                	await self._processEvent(x.event);
                	x.result.resolve();
            	} catch(e) {
% if options.debug:
                	debug(moment().format('hh:mm:ss.SS') + ': ' + self.stateName(self.state) + " : error while processing event " + self.eventName(x.event) + ':\n' + e.stack);
% endif
                	x.result.reject(e);
            	} finally {
                	self._events.shift();
            	}
        	}
    	})();
	}

	async _processEvent(event) {
% if options.debug:
    	debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event));
% endif

    	assert(_.includes(_.values(EVENTS), event), 'Unknown event ' + event + ' passed');

    	if (event === EVENTS.Init) {
        	this.state = STATES.${machine.initialState};
% for a in machine.state(machine.initialState).incomeActions:
	% if options.debug:
        	debug(moment().format('hh:mm:ss.SS') + ': ${a.type} ${a.name}');
   	% endif
        	${formatAction(a)}
% endfor
% if options.debug:
    		debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
        	return;
    	}

    	switch (this.state) {
% for s in machine.states:
    		case STATES.${capitalCase(s.name)}:
	% for (i, t) in enumerate(s.transitions):
        		// ${t.name}
        % if i == 0:
        		if (${parseCondition(t.condition)}) {
        % else:
        		else if (${parseCondition(t.condition)}) {
        % endif
        % if t.destination:
        	% for a in s.exitingActions:
            	% if options.debug:
            		debug(moment().format('hh:mm:ss.SS') + "   - ${a.name}");
                % endif
            		${formatAction(a)}
            % endfor
        % endif
        % for a in t.actions:
        	% if options.debug:
            		debug(moment().format('hh:mm:ss.SS') + '   - ${a.name}');
            % endif
            		${formatAction(a)}
        % endfor
        % if t.destination:
        	% if options.debug:
            		debug(moment().format('hh:mm:ss.SS') + ': ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}');
            % endif
            		this.state = STATES.${t.destination.name};
            % for a in t.destination.incomeActions:
            	% if options.debug:
            		debug(moment().format('hh:mm:ss.SS') + '   - ${a.name}');
                % endif
            		${formatAction(a)}
            % endfor
        % endif
        		}
    % endfor
    %if options.debug:
    	%if len(s.transitions) > 0:
        		else {
            		debug(moment().format('hh:mm:ss.SS') + ': impasse state for event:' + this.eventName(event));
        		}
        %else:
        		debug(moment().format('hh:mm:ss.SS') + ': impasse state for event:' + this.eventName(event));
        %endif
    %endif
        		break;
				
% endfor
    	}

% if options.debug:
    	debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
	}
}	

module.exports = FSM;