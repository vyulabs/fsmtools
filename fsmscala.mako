# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event == %s.Event.%s)' % (machine.name, capitalCase(machine.event(s.group()).name))
        except KeyError:
            return 'delegate.%s(params)' % machine.input(s.group()).name
    try:
        return 'event == %s.Event.%s' % (machine.name, capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatAction(a):
    if a.type == 'output':
        return 'delegate.%s(params)' % a.name
    else:
        return 'processEvent(%s.Event.%s, params)' % (machine.name, capitalCase(a.name))

%>\
package ${machine.baseClass}

import scala.collection.mutable.Queue

% if options.debug:
import akka.event.LoggingAdapter
% endif

class ${machine.name}[Parameters](delegate: ${machine.name}.Delegate[Parameters])\
% if options.debug:
(implicit protected val log: LoggingAdapter)\
% endif
 {

    // MARK: - FSM

    private val eventQueue = new Queue[(${machine.name}.Event.Event, Parameters)]
    private var currentState_ : ${machine.name}.State.State = ${machine.name}.State.${capitalCase(machine.initialState)}

    def currentState : ${machine.name}.State.State = currentState_

    def processEvent(event:${machine.name}.Event.Event, params: Parameters): Unit = {
    	val __empty = eventQueue.isEmpty
      eventQueue.enqueue( (event, params) )
    	if (__empty) {
        while (eventQueue.nonEmpty) {
          val (ev, pars) = eventQueue.front
          processEvent_(ev, pars)
          eventQueue.dequeue
    		}
    	}
    }

    private def processEvent_(event:${machine.name}.Event.Event, params: Parameters): Unit = {
      // import ${machine.name}.{Event,State}
% if options.debug:
      log.debug(s"${machine.name}($delegate): $currentState : event $event")
% endif

      event match {
        case ${machine.name}.Event._Init =>
          currentState_ = ${machine.name}.State.${capitalCase(machine.initialState)}
          % for a in machine.state(machine.initialState).incomeActions:
            % if options.debug:
            log.debug(s"${machine.name}($delegate): ${a.type} ${a.name}($params)")
            % endif
          ${formatAction(a)}
          % endfor
          return
        case _ =>
      }


      currentState_ match {
        % for s in machine.states:
            % for (i, t) in enumerate(s.transitions):
        // ${t.name}
        case ${machine.name}.State.${capitalCase(s.name)} if ${parseCondition(t.condition)} =>
                % if t.destination:
                    % if options.debug:
          log.debug(s"${machine.name}($delegate): ${s.name} -> ${t.destination.name}: ${t.name}")
                    % endif
          currentState_ = ${machine.name}.State.${capitalCase(t.destination.name)}
                  % for a in s.exitingActions:
                    % if options.debug:
          log.debug(s"${machine.name}($delegate): ${a.type} ${a.name}($params)")
                    % endif
          ${formatAction(a)}
                  % endfor
                % endif
                % for a in t.actions:
                    % if options.debug:
          log.debug(s"${machine.name}($delegate): ${a.type} ${a.name}($params)")
                    % endif
          ${formatAction(a)}
                % endfor
                % if t.destination:
                    % for a in t.destination.incomeActions:
                      % if options.debug:
          log.debug(s"${machine.name}($delegate): ${a.type} ${a.name}($params)")
                      % endif
          ${formatAction(a)}
                    % endfor
                % endif
            % endfor
        % endfor
    		case _ =>
            % if options.debug:
    			log.debug(s"- ${machine.name}($delegate): $currentState: impasse with event $event")
            % endif
      }
    }
}

object ${machine.name} {
  abstract class Delegate[Parameters] {
    // MARK: Inputs
% for i in machine.inputs:
    % if i.comment:
    /**
      % for l in wrap(i.comment, 80):
        ${l}
      % endfor
    */
    % endif
    def ${i.name}(params: Parameters): Boolean
% endfor

    // MARK: Outputs
% for o in machine.outputs:
    %if o.comment:
    /**
    % for l in wrap(o.comment, 80):
        ${l}
    % endfor
    */
    % endif
    def ${o.name}(params: Parameters)
% endfor
  }

    // MARK: States
  object State extends Enumeration {
    type State = Value
    % for (index, s) in enumerate(machine.states):
    val ${capitalCase(s.name)} : State = Value
    % endfor
  }

  object Event extends Enumeration {
    type Event = Value
    val _Init : Event = Value
    % for (index, s) in enumerate(machine.events):
    val ${capitalCase(s.name)} : Event = Value
    % endfor
  }
}