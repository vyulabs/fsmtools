# coding: utf-8
<%!
from textwrap import wrap
%>@import Foundation;

% if options.isAsync:
@class RACSignal;
% endif

typedef NS_ENUM(NSInteger, ${machine.name}State) {
% for s in machine.states[:-1]:
    ${machine.name}State${capitalCase(s.name)},
% endfor
    ${machine.name}State${capitalCase(machine.states[-1].name)}
};

typedef NS_ENUM(NSInteger, ${machine.name}Event) {
    ${machine.name}Event_Init,
% for e in machine.events[:-1]:
    % if e.comment:
    /**
        % for l in wrap(e.comment, 80):
        ${l}
        % endfor
    */
    % endif
    ${machine.name}Event${capitalCase(e.name)},
% endfor
    % if machine.events[-1].comment:
    /**
        % for l in wrap(machine.events[-1].comment, 80):
        ${l}
        % endfor
    */
    % endif
    ${machine.name}Event${capitalCase(machine.events[-1].name)}
};

@protocol ${machine.name}Delegate
% for i in machine.inputs:
    % if i.comment:
    /**
        % for l in wrap(i.comment, 80):
        ${l}
        % endfor
    */
    % endif
- (BOOL) ${i.name}:(id)params;
% endfor

% for o in machine.outputs:
    %if o.comment:
        /**
        % for l in wrap(o.comment, 80):
        ${l}
        % endfor
        */
    % endif
    %if options.isAsync:
- (void) ${o.name}:(id)params completion:(void(^)(void))handler;
    %else:
- (void) ${o.name}:(id)params;
    %endif
% endfor
@end

@interface ${machine.name} : NSObject
#pragma mark -
#pragma mark Driving FSM

- (id) initWithDelegate:(id<${machine.name}Delegate>)delegate;
% if options.isAsync:
- (void) processEvent:(enum ${machine.name}Event)event withParams:(id)params completion:(void(^)(void))completion;
- (RACSignal*) processEvent:(enum ${machine.name}Event)event withParams:(id)params;
% else:
- (void) processEvent:(enum ${machine.name}Event)event withParams:(id)params;
% endif

@property (assign, readonly) ${machine.name}State currentState;
+ (NSString*) stateName:(enum ${machine.name}State)state;
+ (NSString*) eventName:(enum ${machine.name}Event)event;
@end
