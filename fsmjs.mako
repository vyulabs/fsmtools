# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event == this.Events.%s)' % (capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '(yield this.%s())' % machine.input(s.group()).name
    try:
        return 'event == this.Events.%s' % (capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatAction(a):
    if a.type == 'output':
        return 'yield this.%s();' % a.name
    else:
        return 'yield this.processEvent(this.Events.%s);' % (machine.name, capitalCase(a.name))

def fromCamelCase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
%>\
%if options.debug:
var moment = require('moment');
var debug = require('debug')('${machine.name.lower()}');
%endif

var _ = require('lodash');
var co = require('co');
var Promise = require('bluebird');
// var Queue = require('co-queue');
var assert = require('assert');
function FSM(state) {
    this.state = !state ? this.States.${machine.initialState} : state;
    this._events = [];
}

FSM.prototype.States = {
% for (index, s) in enumerate(machine.states):
    % if index == len(machine.states) - 1:
    ${capitalCase(s.name)}: '${fromCamelCase(s.name)}'
    % else:
    ${capitalCase(s.name)}: '${fromCamelCase(s.name)}',
    %endif
% endfor
};

FSM.prototype.Events = {
    Init: 0,
% for (index, s) in enumerate(machine.events):
    % if index == len(machine.events) - 1:
    ${capitalCase(s.name)}: ${index + 1}
    % else:
    ${capitalCase(s.name)}: ${index + 1},
    %endif
% endfor
};

% for (index, s) in enumerate(machine.inputs):
FSM.prototype.${s.name} = function * () {
    return yield Promise.resolve(false);
};
% endfor

% for (index, s) in enumerate(machine.outputs):
FSM.prototype.${s.name} = function * () {
    return yield Promise.resolve();
};
% endfor

% if options.debug:
FSM.prototype.eventName = function(event) {
    switch (event) {
        case 0:
            return '<Initialization event>';
    % for (index, s) in enumerate(machine.events):
        case ${index + 1}:
            return '${s.name}';
    % endfor
    }
    return "Unknown (" + event + ")";
};

FSM.prototype.stateName = function(state) {
    switch (state) {
    % for (index, s) in enumerate(machine.states):
        case this.States.${s.name}:
            return '${s.name}';
    % endfor
    }
    return "Unknown (" + state + ")";
};
% endif

function defer() {
    var resolve, reject;
    var promise = new Promise(function() {
        resolve = arguments[0];
        reject = arguments[1];
    });
    return {
        resolve: resolve,
        reject: reject,
        promise: promise
    };
}

FSM.prototype.processEvent = function (event) {
    var empty = _.isEmpty(this._events);

    var result = defer();
    this._events.push({event:event, result:result});
    if (empty) {
        this._processQueue();
        return result.promise;
    }
    else
        return Promise.resolve();
};

FSM.prototype._processQueue = function () {
    var self = this;
    co(function * _handle_queue() {
        while (!_.isEmpty(self._events)) {
            var x = self._events[0];
            try {
                yield self._processEvent(x.event);
                x.result.resolve();
            }
            catch(e) {
% if options.debug:
                debug(moment().format('hh:mm:ss.SS') + ': ' + self.stateName(self.state) + " : error while processing event " + self.eventName(x.event) + ':\n' + e.stack);
% endif
                x.result.reject(e);
            }
            finally {
                self._events.shift();
            }
        }
    });
};

FSM.prototype._processEvent = function * (event) {
% if options.debug:
    debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event));
% endif

    assert(_.includes(_.values(this.Events), event), 'Unknown event ' + event + ' passed');

    if (event == this.Events.Init) {
        this.state = this.States.${machine.initialState};
        % for a in machine.state(machine.initialState).incomeActions:
            % if options.debug:
        debug(moment().format('hh:mm:ss.SS') + ': ${a.type} ${a.name}');
            % endif
        ${formatAction(a)}
        % endfor
% if options.debug:
    debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
        return;
    }

    switch (this.state) {
    % for s in machine.states:
    case this.States.${capitalCase(s.name)}:
        % for (i, t) in enumerate(s.transitions):
        // ${t.name}
            % if i == 0:
        if (${parseCondition(t.condition)}) {
            % else:
        else if (${parseCondition(t.condition)}) {
            % endif
            % if t.destination:
                % for a in s.exitingActions:
                    % if options.debug:
            debug(moment().format('hh:mm:ss.SS') + "   - ${a.name}");
                    % endif
            ${formatAction(a)}
                % endfor
            % endif
            % for a in t.actions:
                    % if options.debug:
            debug(moment().format('hh:mm:ss.SS') + '   - ${a.name}');
                    % endif
            ${formatAction(a)}
            % endfor
            % if t.destination:
                    % if options.debug:
            debug(moment().format('hh:mm:ss.SS') + ': ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}');
                    % endif
            this.state = this.States.${t.destination.name};
                % for a in t.destination.incomeActions:
                    % if options.debug:
            debug(moment().format('hh:mm:ss.SS') + '   - ${a.name}');
                    % endif
            ${formatAction(a)}
                % endfor
            % endif
        }
        % endfor
        %if options.debug:
            %if len(s.transitions) > 0:
        else {
            debug(moment().format('hh:mm:ss.SS') + ': impasse state for event:' + this.eventName(event));
        }
            %else:
        debug(moment().format('hh:mm:ss.SS') + ': impasse state for event:' + this.eventName(event));
            %endif

        %endif
        break;
    % endfor
    }

% if options.debug:
    debug(moment().format('hh:mm:ss.SS') + ': ' + this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
};

module.exports = FSM;