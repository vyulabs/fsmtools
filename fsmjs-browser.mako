# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event == this.Events.%s)' % (capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '(this.delegate.%s(params))' % machine.input(s.group()).name
    try:
        return 'event == this.Events.%s' % (capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatAction(a):
    if a.type == 'output':
        return 'this.delegate.%s(params);' % a.name
    else:
        return 'this.processEvent(this.Events.%s, params);' % capitalCase(a.name)

def fromCamelCase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
%>\

function ${capitalCase(machine.name)}(delegate) {
    this.state = this.States.${machine.initialState};
    this._events = [];
    this.delegate = delegate;
}

${capitalCase(machine.name)}.prototype.States = {
% for (index, s) in enumerate(machine.states):
    % if index == len(machine.states) - 1:
    ${capitalCase(s.name)}: '${s.name}'
    % else:
    ${capitalCase(s.name)}: '${s.name}',
    %endif
% endfor
};

${capitalCase(machine.name)}.prototype.Events = {
    Init: 0,
% for (index, s) in enumerate(machine.events):
    % if index == len(machine.events) - 1:
    ${capitalCase(s.name)}: ${index + 1}
    % else:
    ${capitalCase(s.name)}: ${index + 1},
    %endif
% endfor
};

${capitalCase(machine.name)}.prototype.eventName = function(event) {
    switch (event) {
        case 0:
            return '<Initialization event>';
    % for (index, s) in enumerate(machine.events):
        case ${index + 1}:
            return '${s.name}';
    % endfor
    }
    return "Unknown (" + event + ")";
};

${capitalCase(machine.name)}.prototype.stateName = function(state) {
    switch (state) {
    % for (index, s) in enumerate(machine.states):
        case this.States.${s.name}:
            return '${s.name}';
    % endfor
    }
    return "Unknown (" + state + ")";
};

${capitalCase(machine.name)}.prototype.processEvent = function (event, params) {
    var empty = this._events.length == 0;

    this._events.push({event: event, params: params});
    if (empty) {
        this._processQueue();
    }
};

${capitalCase(machine.name)}.prototype.isIn = function () {
    var self = this;
    var args = Array.prototype.slice.call(arguments);
    for(var i = 0; i < args.length; ++i) {
        if (args[i] == self.state)
            return true;
    }

    return false;
};


${capitalCase(machine.name)}.prototype._processQueue = function () {
    var self = this;

    while (self._events.length > 0) {
        var x = self._events[0];
        self._processEvent(x.event, x.params);
        self._events.shift();
    }
};

${capitalCase(machine.name)}.prototype._processEvent = function(event, params) {
% if options.debug:
    console.log('${machine.name}: ' + this.stateName(this.state) + " : event " + this.eventName(event));
% endif

//    assert(_.includes(_.values(this.Events), event), 'Unknown event ' + event + ' passed');

    if (event == this.Events.Init) {
        this.state = this.States.${machine.initialState};
        % for a in machine.state(machine.initialState).incomeActions:
            % if options.debug:
        console.log('${machine.name}: ${a.type} ${a.name}');
            % endif
        ${formatAction(a)}
        % endfor
% if options.debug:
    console.log(this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
        return;
    }

    switch (this.state) {
    % for s in machine.states:
    case this.States.${capitalCase(s.name)}:
        % for (i, t) in enumerate(s.transitions):
        // ${t.name}
            % if i == 0:
        if (${parseCondition(t.condition)}) {
            % else:
        else if (${parseCondition(t.condition)}) {
            % endif
            % if t.destination:
                % if options.debug:
            console.log('${machine.name}: ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}');
                % endif
            this.state = this.States.${t.destination.name};
                % for a in s.exitingActions:
                    % if options.debug:
            console.log("${machine.name}:    - ${a.name}");
                    % endif
            ${formatAction(a)}
                % endfor
            % endif
            % for a in t.actions:
                    % if options.debug:
            console.log('${machine.name}:    - ${a.name}');
                    % endif
            ${formatAction(a)}
            % endfor
            % if t.destination:
                % for a in t.destination.incomeActions:
                    % if options.debug:
            console.log('${machine.name}:    - ${a.name}');
                    % endif
            ${formatAction(a)}
                % endfor
            % endif
        }
        % endfor
        %if options.debug:
            %if len(s.transitions) > 0:
        else {
            console.log('${machine.name}: impasse state for event: ' + this.eventName(event));
        }
            %else:
        console.log('${machine.name}: impasse state for event: ' + this.eventName(event));
            %endif

        %endif
        break;
    % endfor
    }

% if options.debug:
    console.log('${machine.name}: ' + this.stateName(this.state) + " : event " + this.eventName(event) + ' processing complete');
% endif
};

// var fsm = new ${capitalCase(machine.name)}({
% for (index, s) in enumerate(machine.inputs):
//     ${s.name} : function(x) {
//         console.log('stub for ${s.name}, params: ' + JSON.stringify(x));
//         return false;
//     },
% endfor

% for (index, s) in enumerate(machine.outputs):
//     ${s.name}: function(x) {
//         console.log('stub for ${s.name}, params: ' + JSON.stringify(x));
//         return;
//     },
% endfor
// });
