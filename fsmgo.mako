# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event == Event%s)' % (capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '(rc.%s())' % capitalFirst(machine.input(s.group()).name)
    try:
        return 'event == Event%s' % (capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatAction(a):
    if a.type == 'output':
        return 'rc.%s()' % capitalFirst(a.name)
    else:
        return 'rc.processEvent(Event%s);' % (machine.name, capitalCase(a.name))

def fromCamelCase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
	
def capitalFirst(s):
	return s[0].upper() + s[1:]
%>\
import (
	"strconv"
	"time"
)

//
// State
//

type State string

const (
% for (index, s) in enumerate(machine.states):
    State${capitalCase(s.name)} State = "${fromCamelCase(s.name)}"
% endfor
)

func StateName(state State) string {
    switch state {
% for (index, s) in enumerate(machine.states):
    case State${s.name}:
        return "${s.name}"
% endfor
    }
    return "Unknown (" + string(state) + ")"
}

//
// Event
//

type Event int

const (
    EventInit Event = 0
% for (index, s) in enumerate(machine.events):
    Event${capitalCase(s.name)} Event = ${index + 1}
% endfor
)

func EventName(event Event) string {
    switch (event) {
    case 0:
        return "<Initialization event>"
% for (index, s) in enumerate(machine.events):
    case ${index + 1}:
        return "${s.name}"
% endfor
    }
    return "Unknown (" + strconv.Itoa(int(event)) + ")"
}

//
// Fsm
//

type Fsm interface {
% for (index, s) in enumerate(machine.inputs):
	${capitalFirst(s.name)}() bool
% endfor
% for (index, s) in enumerate(machine.outputs):
	${capitalFirst(s.name)}()
% endfor
}

//
// Machine
//

type Machine struct {
    State 		State
	Delegate	Fsm
}

% for (index, s) in enumerate(machine.inputs):
func (rc Machine) ${capitalFirst(s.name)}() bool {
    return rc.Delegate.${capitalFirst(s.name)}()
}
% endfor
% for (index, s) in enumerate(machine.outputs):
func (rc Machine) ${capitalFirst(s.name)}() {
    rc.Delegate.${capitalFirst(s.name)}()
}
% endfor

//
// event processing
//

func (rc *Machine) ProcessEvent(event Event) {
	//TODO
	rc.processEvent(event)
}

func (rc *Machine) processEvent(event Event) {
% if options.debug:
    debug(timeNow() + ": " + StateName(rc.State) + " : event " + EventName(event))
% endif

    if event == EventInit {
        rc.State = State${machine.initialState}
% for a in machine.state(machine.initialState).incomeActions:
    % if options.debug:
        debug(timeNow() + ": ${a.type} ${a.name}")
    % endif
        ${formatAction(a)}
% endfor
% if options.debug:
        debug(timeNow() + ": " + StateName(rc.State) + " : event " + EventName(event) + " processing complete")
% endif
        return
    }

    switch rc.State {
% for s in machine.states:
    case State${capitalCase(s.name)}:
    % for (i, t) in enumerate(s.transitions):
        // ${t.name}
        % if i == 0:
        if (${parseCondition(t.condition)}) {
        % else:
        } else if (${parseCondition(t.condition)}) {
        % endif
        % if t.destination:
            % for a in s.exitingActions:
                % if options.debug:
            debug(timeNow() + "   - ${a.name}")
                % endif
            ${formatAction(a)}
            % endfor
        % endif
        % for a in t.actions:
            % if options.debug:
            debug(timeNow() + "   - ${a.name}")
            % endif
            ${formatAction(a)}
        % endfor
        % if t.destination:
            % if options.debug:
            debug(timeNow() + ": ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}");
            % endif
            rc.State = State${t.destination.name};
            % for a in t.destination.incomeActions:
                % if options.debug:
            debug(timeNow() + "   - ${a.name}");
                % endif
            ${formatAction(a)}
            % endfor
        % endif
    % endfor
    %if options.debug:
        %if len(s.transitions) > 0:
        } else {
            debug(timeNow() + ": impasse state for event:" + EventName(event))
        }
        %else:
        debug(timeNow() + ": impasse state for event:" + EventName(event))
        %endif

    %endif
% endfor
    }

% if options.debug:
    debug(timeNow() + ": " + StateName(rc.State) + " : event " + EventName(event) + " processing complete");
% endif
}

//
// utils
//

func debug(s string) {
	println(s)
}

func timeNow() string {
	return time.Now().Format(time.RFC3339)
}