# coding: utf-8
<%!
    import re
    def links(text):
        def hlink(g):
            return r'\hyperlink{%s}{%s}' % (g.group(), hyp(g.group()))
        return re.sub(r'\w+', hlink, text)

    def ce(text):
        return re.sub('([&{}_$])', r'\\\1', text)
	
    def hyp(text):
        if not text: return ''
        s = text[0]
        for x in text[1:]:
            if x.isupper(): s += r'\-'
            s += x
        return s
%>

<%def name="formatState(s)">
    \hline\multicolumn{1}{|l}{\hypertarget{${s.name}}{\textbf{${s.name}}}} & % 
	\multicolumn{4}{p{20cm}|}{\
	% if s.comment:
		\small{${s.comment | ce}}\
	% endif
	}\\\nopagebreak\hline\nopagebreak
	% for t in s.transitions:
	\small{\texttt{${t.condition | ce,links}}} & \small{${t.name}} & \small{${t.comment | ce}} & \multicolumn{2}{l|}{\
		<%
		if t.destination:
			outlist = s.exitingActions + t.actions + t.destination.incomeActions
			destination = t.destination.name
		else:
			outlist = t.actions
			destination = '<loop>'
		%>\
		\begin{minipage}{7cm}
			\begin{tabular}{p{7cm}}
			$\rightarrow$ \texttt{\hyperlink{${destination}}{${destination}}}${r'\\'}
			% for a in outlist:
				\hspace{0.8cm}\small\texttt{\hyperlink{${a.name}}{${a.name|hyp}}}${r'\\'}
			% endfor
			\end{tabular}
		\end{minipage}}\\\hline
	% endfor
	\hline\hline
</%def>

\documentclass[a4paper,10pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[T2A]{fontenc}
\usepackage[english]{babel}
\usepackage{ltxtable}
\usepackage{graphicx}
\usepackage[colorlinks,linkcolor=black]{hyperref}
\usepackage[margin=1cm,bottom=2cm,landscape]{geometry}

\begin{document}
\section{Events}
Events are distinct letters in finite state machine (automata) alphabet. Event is sent to automata and it changes state performing
some actions if nesessary. Along with Event automata is provided with some external information in params
parameter. These params are passed to inputs and outputs (see below). There are following events defined 
for ${machine.name}:

\begin{description}
% for e in machine.events:
	\item[\hypertarget{${e.name}}{${e.name}}] ${e.comment | ce}
% endfor
\end{description}

\section{Inputs}
Inputs are boolean functions implemented by automata delegate. These functions return YES or NO depending
on environment conditions. There are following inputs defined for ${machine.name}:

\begin{description}
% for e in machine.inputs:
	\item[\hypertarget{${e.name}}{${e.name}}] ${e.comment | ce}
% endfor
\end{description}

\section{Outputs}
Outputs are actions performed by automata delegate in proper situations. Automama calls these outputs according
to rules described on diagram and table below. 

\begin{description}
% for e in machine.outputs:
	\item[\hypertarget{${e.name}}{${e.name}}] ${e.comment | ce}
% endfor
\end{description}

\section{Diagram}
This is a graph of automata states and transitions. Short labels are displayed on diagram as well. Everything
starts at state ${machine.initialState}. Current state is changed according to received events and input values.
These rules are strictly described in table below.
\begin{center}
	\includegraphics[width=0.9\textwidth]{diagram.pdf}
\end{center}

\section{By-state logic description}
\begin{longtable}{|p{5cm}|p{3cm}|p{9cm}|p{1cm}l|}
\hline\multicolumn{1}{|c}{\textbf{Condition}} & %
      \multicolumn{1}{|c}{\textbf{Short desc.}} & %
      \multicolumn{1}{|c}{\textbf{Comment}} & %
      \multicolumn{2}{|c|}{\textbf{Target State and Actions}} \\ \hline
\endhead
\hline
\endfoot
\endlastfoot
<% states = filter(lambda x: x.name != machine.initialState, machine.states) %>\
${formatState(machine.state(machine.initialState))}
% for s in states:
${formatState(s)}
% endfor
\end{longtable}
\end{document}
