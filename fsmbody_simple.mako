# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(state, transition):
    def replacer(s):
        try:
            return '(event == %sEvent%s)' % (machine.name, capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '[delegate %s:params]' % machine.input(s.group()).name
    try:
        return 'event == %sEvent%s' % (machine.name, capitalCase(machine.event(transition.condition.strip()).name))
    except KeyError:
        try:
            return re.sub(r'\b(\w+)\b', replacer, transition.condition)
        except KeyError:
            raise Exception('Cannot parse condition in transition \'%s:%s\'' % (state.name, transition.name))

def formatAction(a):
      if a.type == 'output':
          return '[delegate %s:params];' % a.name
      else:
          return '[self processEvent:%sEvent%s withParams:params];' % (machine.name, capitalCase(a.name))

%>#import "${machine.name}.h"

#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
@import reactivext;

static NSString* debugWithLen(NSObject* o, NSInteger len)
{
    return @"";
}
#endif

@interface ${machine.name}()
@property (nonatomic, assign) ${machine.name}State currentState;
@end

@implementation ${machine.name} {
    __weak id<${machine.name}Delegate> delegate;
    BOOL _running;
}
@synthesize currentState;

- (id) initWithDelegate:(id<${machine.name}Delegate>)aDelegate
{
    if (self = [super init]) {
        delegate = aDelegate;
        currentState = ${machine.name}State${machine.initialState};
        _running = NO;
    }
    return self;
}

- (void) processEventInternal:(${machine.name}Event)event withParams:(NSDictionary*)params
{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
    DDLogDebug(@"- %@: event %@", self, [${machine.name} eventName:event]);
#endif

    if (event == ${machine.name}Event_Init) {
        self.currentState = ${machine.name}State${machine.initialState};
        % for (j, a) in enumerate(machine.state(machine.initialState).incomeActions):
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
        DDLogDebug(@"- %@: ${a.type} ${a.name}:%@", self, debugWithLen(params, 80));
#endif
        ${formatAction(a)}
        % endfor

        return;
    }

    switch (currentState) {
% for s in machine.states:
    case ${machine.name}State${capitalCase(s.name)}:
    % for (i, t) in enumerate(s.transitions):
        // ${t.name}
        % if i == 0:
        if (${parseCondition(s, t)}) {
        % else:
        else if (${parseCondition(s, t)}) {
        % endif
        % if t.destination:
            % for (j, a) in enumerate(s.exitingActions):
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: - ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
            % endfor
        % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: -> keeping state: ${t.name}", self);
#endif
        % endif
        % for (j, a) in enumerate(t.actions):
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
        % endfor
        % if t.destination:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@ ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}", self);
#endif
            self.currentState = ${machine.name}State${t.destination.name};
            % for (j, a) in enumerate(t.destination.incomeActions):
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: ${a.type} ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
            % endfor
        % endif
            break;
        }
    % endfor
        % if len(s.transitions) == 0:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: impasse with event %@", self, [${machine.name} eventName:event]);
#endif
        % else:
        else {
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: impasse with event %@", self, [${machine.name} eventName:event]);
#endif
        }
        % endif
        break;
% endfor
    }
}

- (void) processEvent:(enum ${machine.name}Event)event withParams:(NSDictionary*)params
{
    NSAssert(!_running, @"Recursive entry while processing event");
    _running = YES;
    [self processEventInternal:event
                    withParams:params];
    _running = NO;
}

+ (NSString*) stateName:(enum ${machine.name}State)state
{
    switch (state) {
% for s in machine.states:
    case ${machine.name}State${capitalCase(s.name)}:
        return @"${capitalCase(s.name)}";
% endfor
    }
    return @"Unknown";
}

+ (NSString*) eventName:(enum ${machine.name}Event)event
{
    switch (event) {
    case ${machine.name}Event_Init:
        return @"<Initialization event>";
% for e in machine.events:
    case ${machine.name}Event${capitalCase(e.name)}:
        return @"${capitalCase(e.name)}";
% endfor
    }
    return @"Unknown";
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"${'<%@: %p>: %@'}", NSStringFromClass([self class]), self, [[self class] stateName:self.currentState]];
}
@end
