# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(c):
    def replacer(s):
        try:
            return '(event == %s.Event.%s)' % (machine.name, capitalCase(machine.event(s.group()).name))
        except KeyError:
            return 'delegate.%s(params)' % machine.input(s.group()).name
    try:
        return 'event == %s.Event.%s' % (machine.name, capitalCase(machine.event(c.strip()).name))
    except KeyError:
        return re.sub(r'\b(\w+)\b', replacer, c);

def formatActionStart(a):
    if a.type == 'output':
        return 'delegate.%s(params,new Callback() {\n@Override\npublic void done() {' % a.name
    else:
        return 'processEvent(%s.Event.%s, params);' % (machine.name, capitalCase(a.name))

def formatActionEnd(a):
    if a.type == 'output':
        return '}\n}\n);\n //%s' % a.name
    else:
        return ' '
%>\
package ${machine.baseClass};

import java.util.Queue;
import java.util.concurrent.CountDownLatch;
% if options.debug:
import android.util.Log;
% endif

public class ${machine.name}<Parameters>\
 {
    private class EventWithParams{
		final ${machine.name}.Event event;
		final Parameters parameters;

		public EventWithParams(${machine.name}.Event event,Parameters parameters){
			this.event = event;
			this.parameters = parameters;
		}
	}
	public interface Callback {
        void done();
    }
    public interface Delegate<Parameters> {
    // MARK: Inputs
% for i in machine.inputs:
    % if i.comment:
    /**
      % for l in wrap(i.comment, 80):
        ${l}
      % endfor
    */
    % endif
        boolean ${i.name}(Parameters params);
% endfor

    // MARK: Outputs
% for o in machine.outputs:
    %if o.comment:
    /**
    % for l in wrap(o.comment, 80):
        ${l}
    % endfor
    */
    % endif
        void ${o.name}(Parameters params,Callback callOnDone);
% endfor
		void currentStateChanged(State state);
    }

    // MARK: States
    public enum State {
    % for (index, s) in enumerate(machine.states):
        ${capitalCase(s.name)} ,
    % endfor
    }

    public enum Event  {
        _Init,
    % for (index, s) in enumerate(machine.events):
        ${capitalCase(s.name)},
    % endfor
    }

    private final Queue<EventWithParams> eventQueue = new java.util.LinkedList<>();
    private ${machine.name}.State currentState_ = ${machine.name}.State.${capitalCase(machine.initialState)};

	private final ${machine.name}.Delegate<Parameters> delegate;
	<% logtag = machine.name[:23] %>
% if options.debug:
	protected static final String LOG_TAG = "${logtag}";
% endif

    ${machine.name}(${machine.name}.Delegate<Parameters> delegate) {
		this.delegate = delegate;
	}

    // MARK: - FSM
    public ${machine.name}.State getCurrentState() { return currentState_; }

    private boolean running__ = false;
    private boolean stopped__ = false;
    private final Object runningStateLock__ = new Object();

    private void processEvents() {
        try {
            synchronized (runningStateLock__) {
                stopped__ = false;
                running__ = true;
                runningStateLock__.notifyAll();
            };

            for (; ; ) {
                synchronized (runningStateLock__) {
                    if (!running__) {
                        return;
                    }
                }
                EventWithParams e = null;
                synchronized (eventQueue) {
                    if (eventQueue.isEmpty()) {
                        try {
                            eventQueue.wait();
                        } catch (InterruptedException ex) {
                % if options.debug:
                            Log.d(LOG_TAG, "Event processor thread interrupted, breaking event queue loop");
                % endif
							return;
                        }
                    } else {
                        e = eventQueue.poll();
					}
				}
				if(e != null){
				% if options.debug:
                		Log.d(LOG_TAG, "Event " + e.event.name() + " at " + currentState_.name());
                % endif
				        final CountDownLatch block = new CountDownLatch(1);
                        processEvent_(e, new Callback() {
                            @Override
                            public void done() {
				% if options.debug:
                				Log.d(LOG_TAG, "New state: " + currentState_.name());
                % endif
				                block.countDown();
                            }
                        });
                        try {
                            block.await();
                        } catch (InterruptedException ex) {
                            Log.d(LOG_TAG,"Fsm thread interrupted");
                            return;
                        }
                }
            }
        } finally {
            synchronized (runningStateLock__) {
                running__ = false;
                stopped__ = true;
                runningStateLock__.notifyAll();
            }
        }
    }

    public void start() {
        synchronized (runningStateLock__) {
            if (running__)
                return;

            Thread fsmRunner = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        processEvents();
                    } catch (Throwable ex) {
				% if options.debug:
                        Log.d(LOG_TAG, "Exception on event processing", ex);
				% endif
                    }
                }
            });
            eventQueue.clear();
            eventQueue.add(new EventWithParams(Event._Init, null));

            fsmRunner.setName("${machine.name} event handler thread");
            fsmRunner.setDaemon(true);
            fsmRunner.start();

            while (!running__) {
                try {
                    runningStateLock__.wait();
                } catch (InterruptedException ex) {
                    return;
                }
            }
        }
    }

    public void stop() {
        synchronized (runningStateLock__) {
            if (stopped__)
                return;
            running__ = false;
        }
        synchronized (eventQueue) {
            eventQueue.clear();
            eventQueue.notifyAll();
        }
        synchronized (runningStateLock__) {
            while (!stopped__) {
                try {
                    runningStateLock__.wait();
                } catch (InterruptedException ex) {
                    return;
                }
            }
            currentState_ = ${machine.name}.State.${capitalCase(machine.initialState)};
        }
    }

    public void processEvent(${machine.name}.Event event) {
        processEvent(event,null);
    }

    public void processEvent(${machine.name}.Event event, Parameters params) {
        synchronized (runningStateLock__) {
            if (!running__) {
                return;
            }
            synchronized (eventQueue) {
                eventQueue.add(new EventWithParams(event, params));
                eventQueue.notifyAll();
            }
        }
    }

	private void setCurrentState(State state) {
        currentState_ = state;
        delegate.currentStateChanged(state);
    }

    private void processEvent_(EventWithParams eventParams, final Callback callback){
% if options.debug:
        Log.d(LOG_TAG," " + getCurrentState().toString() + " : " + eventParams.event.toString());
% endif
        final Event event = eventParams.event;
		final Parameters params = eventParams.parameters;
        switch(event) {
            case _Init:
                setCurrentState(${machine.name}.State.${capitalCase(machine.initialState)});
          <%
					actions = 0
				%>
		  % for a in machine.state(machine.initialState).incomeActions:
            % if options.debug:
                Log.d(LOG_TAG,"${a.type} ${a.name}");
            % endif
                ${formatActionStart(a)};
				<% actions += 1 %>
          % endfor
		  % if actions >= 0:
		  callback.done();
		  % endif
		  % for a in reversed(machine.state(machine.initialState).incomeActions):
                    ${formatActionEnd(a)}
          % endfor
                return;
            default:
			//ignore
        }
        switch(currentState_) {
        % for s in machine.states:
		    case ${capitalCase(s.name)}:
    	        // ${s.name}
            % for (i, t) in enumerate(s.transitions):
			% if (i > 0):
 else if \
			% else:
                if \
			% endif
(${parseCondition(t.condition)}) {
       	        // ${t.name}
				<%
					actions = 0
				%>
                % if t.destination:
                 % if options.debug:
                  Log.d(LOG_TAG,"${s.name} -> ${t.destination.name}: ${t.name}");
                 % endif
                 setCurrentState(${machine.name}.State.${capitalCase(t.destination.name)});
                  % for a in s.exitingActions:
                	% if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - calling");
                    % endif
                    ${formatActionStart(a)}
				    % if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - done");
                    % endif
                    <%
					 actions += 1
					%>
                  % endfor
				% endif

                % for a in t.actions:
                	% if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - calling");
                    % endif
                    ${formatActionStart(a)}
				    % if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - done");
                    % endif
					<%
					 actions += 1
					%>
                % endfor
				% if t.destination:
                  % for a in t.destination.incomeActions:
					% if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - calling");
                    % endif
					${formatActionStart(a)}
					% if options.debug:
                    Log.d(LOG_TAG,"${a.type} ${a.name} - done");
                    % endif
					<%
					 actions += 1
					%>
                  % endfor
                % endif
				callback.done();
				% if t.destination:
    				% for a in reversed(t.destination.incomeActions):
                    ${formatActionEnd(a)}
                    % endfor
                % endif
				% for a in reversed(t.actions):
                    ${formatActionEnd(a)}
                % endfor
				% for a in reversed(s.exitingActions):
                    ${formatActionEnd(a)}
                % endfor
			    }\
            % endfor
		else callback.done();
			    break;
        % endfor
		    default:
          % if options.debug:
    	        Log.d(LOG_TAG," " + getCurrentState().toString() + ": impasse with event " + eventParams.event.toString());
          % endif
                callback.done();
        }
    }
}

