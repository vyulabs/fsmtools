# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(state, transition):
    def replacer(s):
        try:
            return '(event == %s)' % capitalCase(machine.event(s.group()).name)
        except KeyError:
            return 'm_delegate->%s(params)' % machine.input(s.group()).name
    try:
        return 'event == %s' % capitalCase(machine.event(transition.condition.strip()).name)
    except KeyError:
        try:
            return re.sub(r'\b(\w+)\b', replacer, transition.condition)
        except KeyError:
            raise Exception('Cannot parse condition in transition \'%s:%s\'' % (state.name, transition.name))

def formatAction(a):
      if a.type == 'output':
          return 'm_delegate->%s(params);' % a.name
      else:
          return 'processEvent(%s, params);' % capitalCase(a.name)

%>#include <QtCore>
#include "${machine.name}.h"
%if options.debug:
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(fsmdebug)
Q_LOGGING_CATEGORY(fsmdebug, "fsm.${machine.name}")
%endif

${machine.name}::${machine.name}(${machine.name}Delegate * delegate)
    : m_delegate(delegate)
    , m_currentState(${machine.initialState})
{
}

${machine.name}::State ${machine.name}::currentState() const
{
    return m_currentState;
}

void ${machine.name}::processEventInternal(Event event, const QVariant& params)
{
%if options.debug:
    qCDebug(fsmdebug) << "event: " << eventName(event).c_str() << ", current state: " << stateName(currentState()).c_str();
%endif

    if (event == Event_Init) {
        m_currentState = ${machine.initialState};
        % for (j, a) in enumerate(machine.state(machine.initialState).incomeActions):
%if options.debug:
        qCDebug(fsmdebug) << "${a.name}(" << params << ")";
%endif
        ${formatAction(a)}
        % endfor

        return;
    }

    switch (m_currentState) {
% for s in machine.states:
    case ${capitalCase(s.name)}:
    % for (i, t) in enumerate(s.transitions):
        // ${t.name}
        % if i == 0:
        if (${parseCondition(s, t)}) {
        % else:
        else if (${parseCondition(s, t)}) {
        % endif
        % if t.destination:
            % for (j, a) in enumerate(s.exitingActions):
%if options.debug:
            qCDebug(fsmdebug) << "${a.name}(" << params << ")";
%endif
            ${formatAction(a)}
            % endfor
        % else:
%if options.debug:
            qCDebug(fsmdebug) << "keeping state ${capitalCase(s.name)}: ${t.name}";
%endif
        % endif
        % for (j, a) in enumerate(t.actions):
%if options.debug:
            qCDebug(fsmdebug) << "${a.name}(" << params << ")";
%endif
            ${formatAction(a)}
        % endfor
        % if t.destination:
%if options.debug:
            qCDebug(fsmdebug) << "${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}";
%endif
            m_currentState = ${t.destination.name};
            % for (j, a) in enumerate(t.destination.incomeActions):
%if options.debug:
            qCDebug(fsmdebug) << "${a.type} ${a.name}(" << params << ")";
%endif
            ${formatAction(a)}
            % endfor
        % endif
            break;
        }
    % endfor
        % if len(s.transitions) == 0:
%if options.debug:
        qCDebug(fsmdebug) << "impasse with event" << eventName(event).c_str();
%endif
        % else:
        else {
%if options.debug:
            qCDebug(fsmdebug) << "impasse with event" << eventName(event).c_str();
%endif
        }
        % endif
        break;
% endfor
    }
}

void ${machine.name}::processEvent(Event e, const QVariant& params)
{
    bool isEmpty = m_queue.isEmpty();
    m_queue.enqueue(qMakePair(e, params));

    if (isEmpty) {
        while(!m_queue.isEmpty()) {
            auto x = m_queue.dequeue();
            processEventInternal(x.first, x.second);
        }
    }
}

std::string ${machine.name}::stateName(State state)
{
    switch (state) {
% for s in machine.states:
    case ${capitalCase(s.name)}:
        return "${capitalCase(s.name)}";
% endfor
    }
    return "Unknown";
}

std::string ${machine.name}::eventName(Event event)
{
    switch (event) {
    case Event_Init:
        return "<Initialization event>";
% for e in machine.events:
    case ${capitalCase(e.name)}:
        return "${capitalCase(e.name)}";
% endfor
    }
    return "Unknown";
}
