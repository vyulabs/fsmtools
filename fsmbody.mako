# coding: utf-8
<%!
from textwrap import wrap
import re
%><%
def parseCondition(state, transition):
    def replacer(s):
        try:
            return '(event == %sEvent%s)' % (machine.name, capitalCase(machine.event(s.group()).name))
        except KeyError:
            return '[self->delegate %s:params]' % machine.input(s.group()).name
    try:
        return 'event == %sEvent%s' % (machine.name, capitalCase(machine.event(transition.condition.strip()).name))
    except KeyError:
        try:
            return re.sub(r'\b(\w+)\b', replacer, transition.condition)
        except KeyError:
            raise Exception('Cannot parse condition in transition \'%s:%s\'' % (state.name, transition.name))

def formatAction(a):
    if options.isAsync:
        if a.type == 'output':
            return '[self->delegate %s:params completion:^{' % a.name
        else:
            return '[self processEvent:%sEvent%s withParams:params completion:^{' % (machine.name, capitalCase(a.name))
    else:
        if a.type == 'output':
            return '[self->delegate %s:params];' % a.name
        else:
            return '[self processEvent:%sEvent%s withParams:params];' % (machine.name, capitalCase(a.name))

%>#import "${machine.name}.h"

% if options.isAsync:
@import ReactiveObjC;
% endif

#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
@import reactivext;

static NSString* debugWithLen(NSObject* o, NSInteger len)
{
    return @"";
}
#endif

@interface ${machine.name}()
@property (nonatomic, assign) ${machine.name}State currentState;
@end

@implementation ${machine.name} {
    __weak id<${machine.name}Delegate> delegate;
    NSMutableArray * eventQueue;
}
@synthesize currentState;

- (id) initWithDelegate:(id<${machine.name}Delegate>)aDelegate
{
    if (self = [super init]) {
        delegate = aDelegate;
        currentState = ${machine.name}State${machine.initialState};
        eventQueue = [NSMutableArray arrayWithCapacity:40];
    }
    return self;
}

% if options.isAsync:
- (void) processEventInternal:(${machine.name}Event)event withParams:(id)params completion:(void(^)(void))completion
% else:
- (void) processEventInternal:(${machine.name}Event)event withParams:(id)params
% endif
{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
    DDLogDebug(@"- %@: event %@", self, [${machine.name} eventName:event]);
    NSAssert([NSThread isMainThread], @"State machine events should go only in main thread!");
#endif

    if (event == ${machine.name}Event_Init) {
        self.currentState = ${machine.name}State${machine.initialState};
        % for (j, a) in enumerate(machine.state(machine.initialState).incomeActions):
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
        DDLogDebug(@"- %@: ${a.type} ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            % if options.isAsync:
        ${'    ' * j}dispatch_async(dispatch_get_main_queue(), ^{
            ${'    ' * j}${formatAction(a)}
            % else:
        ${formatAction(a)}
            % endif
        % endfor
        % if options.isAsync:
<% totalActions = len(machine.state(machine.initialState).incomeActions) %>\
            ${'    ' * totalActions}dispatch_async(dispatch_get_main_queue(), completion);
            % for i in reversed(range(totalActions)):
            ${'    ' * i}}];
        ${'    ' * i}});
            % endfor
        % endif

        return;
    }

    switch (currentState) {
% for s in machine.states:
    case ${machine.name}State${capitalCase(s.name)}:
    % for (i, t) in enumerate(s.transitions):
        // ${t.name}
        % if i == 0:
        if (${parseCondition(s, t)}) {
        % else:
        else if (${parseCondition(s, t)}) {
        % endif
        % if t.destination:
            % for (j, a) in enumerate(s.exitingActions):
                % if options.isAsync:
            ${'    ' * j}dispatch_async(dispatch_get_main_queue(), ^{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
                ${'    ' * j}DDLogDebug(@"- %@: - ${a.name}:%@", self, debugWithLen(params, 80));
#endif
                ${'    ' * j}${formatAction(a)}
                % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: - ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
                % endif
            % endfor
        % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: -> keeping state: ${t.name}", self);
#endif
        % endif
        % for (j, a) in enumerate(t.actions):
                % if options.isAsync:
            ${'    ' * (j + len(s.exitingActions))}dispatch_async(dispatch_get_main_queue(), ^{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
                ${'    ' * (j + len(s.exitingActions))}DDLogDebug(@"- %@: ${a.name}:%@", self, debugWithLen(params, 80));
#endif
                ${'    ' * (j + len(s.exitingActions))}${formatAction(a)}
                % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
                % endif
        % endfor
        % if t.destination:
                % if options.isAsync:
            ${'    ' * (len(s.exitingActions) + len(t.actions))}dispatch_async(dispatch_get_main_queue(), ^{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
                ${'    ' * (len(s.exitingActions) + len(t.actions))}DDLogDebug(@"- %@ -> ${t.destination.name}: ${t.name}", self);
#endif
                ${'    ' * (len(s.exitingActions) + len(t.actions))}self.currentState = ${machine.name}State${t.destination.name};
                % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@ ${capitalCase(s.name)} -> ${t.destination.name}: ${t.name}", self);
#endif
            self.currentState = ${machine.name}State${t.destination.name};
                % endif
            % for (j, a) in enumerate(t.destination.incomeActions):
                % if options.isAsync:
                    ${'    ' * (j + len(s.exitingActions) + len(t.actions))}dispatch_async(dispatch_get_main_queue(), ^{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
                        ${'    ' * (j + len(s.exitingActions) + len(t.actions))}DDLogDebug(@"- %@: ${a.type} ${a.name}:%@", self, debugWithLen(params, 80));
#endif
                        ${'    ' * (j + len(s.exitingActions) + len(t.actions))}${formatAction(a)}
                % else:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: ${a.type} ${a.name}:%@", self, debugWithLen(params, 80));
#endif
            ${formatAction(a)}
                % endif
            % endfor
        % endif
        % if options.isAsync:
            <% totalActions = len(s.exitingActions) + len(t.actions) + (len(t.destination.incomeActions) if t.destination else 0) %>
                    ${'    ' * totalActions}dispatch_async(dispatch_get_main_queue(), completion);
            % for i in reversed(range(len(t.destination.incomeActions) if t.destination else 0)):
                ${'    ' * (i + len(s.exitingActions) + len(t.actions))}}];
            ${'    ' * (i + len(s.exitingActions) + len(t.actions))}});
            % endfor
            % if t.destination:
                ${'    ' * (len(s.exitingActions) + len(t.actions))}});
                % for i in reversed(range(len(s.exitingActions) + len(t.actions))):
                    ${'    ' * i}}];
                ${'    ' * i}});
                % endfor
            % else:
                % for i in reversed(range(len(t.actions))):
                    ${'    ' * i}}];
                ${'    ' * i}});
                % endfor
            % endif
        % endif
            break;
        }
    % endfor
        % if len(s.transitions) == 0:
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: impasse with event %@", self, [${machine.name} eventName:event]);
#endif
            %if options.isAsync:
            dispatch_async(dispatch_get_main_queue(), completion);
            % endif
        % else:
        else {
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
            DDLogDebug(@"- %@: impasse with event %@", self, [${machine.name} eventName:event]);
#endif
            % if options.isAsync:
            dispatch_async(dispatch_get_main_queue(), completion);
            % endif
        }
        % endif
        break;
% endfor
    }
}

% if options.isAsync:
- (void) _processQueue
{
    __weak ${machine.name} * _wself = self;
    if (eventQueue.count > 0) {
        NSDictionary * dic = [eventQueue objectAtIndex:0];
        [self processEventInternal:[[dic valueForKey:@"event"] intValue]
                        withParams:[dic valueForKey:@"params"]
                        completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                id completion = dic[@"completion"];
                if (completion != nil && completion != NSNull.null) {
                    void (^compl)(void) = completion;
                    compl();
                }

                ${machine.name} * _sself = _wself;
                if (_sself) {
                    [_sself->eventQueue removeObjectAtIndex:0];
                    [_sself _processQueue];
                }
            });
        }];
    }
}
- (void) processEvent:(enum ${machine.name}Event)event withParams:(id)params completion:(void(^)(void))completion
{
    __weak ${machine.name} * _wself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        ${machine.name} * _sself = _wself;
        if (!_sself) {
            if (completion) completion();
            return;
        }
        BOOL __empty = _sself->eventQueue.count == 0;
        NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithInt:event], @"event",
            [completion copy] ?: [NSNull null], @"completion",
            params, @"params", nil];
        [_sself->eventQueue addObject:dic];
        if (__empty) {
            [_sself _processQueue];
        }
    });
}

- (RACSignal*) processEvent:(enum ${machine.name}Event)event withParams:(id)params
{
    RACReplaySubject * subj = [RACReplaySubject replaySubjectWithCapacity:1];
    [self processEvent:event withParams:params completion:^{
        [subj sendCompleted];
    }];
    return subj;
}
% else:
- (void) processEvent:(enum ${machine.name}Event)event withParams:(id)params
{
#if defined(DEBUG) && !defined(FSMDEBUG_SILENCE_${machine.name.upper()})
    NSAssert([NSThread isMainThread], @"State machine events should go only in main thread! event: %@, state: %@",
                                      [${machine.name} eventName:event],
                                      [${machine.name} stateName:currentState]);
#endif
    BOOL __empty = eventQueue.count == 0;
    NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
        [NSNumber numberWithInt:event], @"event",
        params, @"params", nil];
    [eventQueue addObject:dic];
    if (__empty) {
        while (eventQueue.count > 0) {
            dic = [eventQueue objectAtIndex:0];
            [self processEventInternal:[[dic valueForKey:@"event"] intValue]
                            withParams:[dic valueForKey:@"params"]];
            [eventQueue removeObjectAtIndex:0];
        }
    }
}
% endif


+ (NSString*) stateName:(enum ${machine.name}State)state
{
    switch (state) {
% for s in machine.states:
    case ${machine.name}State${capitalCase(s.name)}:
        return @"${capitalCase(s.name)}";
% endfor
    }
    return @"Unknown";
}

+ (NSString*) eventName:(enum ${machine.name}Event)event
{
    switch (event) {
    case ${machine.name}Event_Init:
        return @"<Initialization event>";
% for e in machine.events:
    case ${machine.name}Event${capitalCase(e.name)}:
        return @"${capitalCase(e.name)}";
% endfor
    }
    return @"Unknown";
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"${'<%@: %p>: %@'}", NSStringFromClass([self class]), self, [[self class] stateName:self.currentState]];
}
@end
