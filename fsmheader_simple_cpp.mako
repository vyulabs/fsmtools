# coding: utf-8
<%!
from textwrap import wrap
%>#include <QVariant>

class ${machine.name}Delegate {
public:\
% for i in machine.inputs:
    % if i.comment:
    /**
        % for l in wrap(i.comment, 80):
        ${l}
        % endfor
    */
    % endif
    virtual bool ${i.name}(const QVariant& params) = 0;
% endfor

% for o in machine.outputs:
    %if o.comment:
        /**
        % for l in wrap(o.comment, 80):
        ${l}
        % endfor
        */
    % endif
    virtual void ${o.name}(const QVariant& params) = 0;
% endfor
};

class ${machine.name} {
public:
    enum State {
% for s in machine.states[:-1]:
        ${capitalCase(s.name)},
% endfor
        ${capitalCase(machine.states[-1].name)}
    };

    enum Event {
        Event_Init,
% for e in machine.events[:-1]:
    % if e.comment:
        /**
            % for l in wrap(e.comment, 80):
            ${l}
            % endfor
        */
    % endif
        ${capitalCase(e.name)},
% endfor
% if machine.events[-1].comment:
        /**
            % for l in wrap(machine.events[-1].comment, 80):
            ${l}
            % endfor
        */
% endif
        ${capitalCase(machine.events[-1].name)}
    };

    ${machine.name}(${machine.name}Delegate * delegate);
    void processEvent(Event e, const QVariant& params = QVariant());
    State currentState() const;
    static std::string stateName(State s);
    static std::string eventName(Event e);

private:
    void processEventInternal(Event e, const QVariant& params);
    ${machine.name}Delegate * m_delegate;
    State m_currentState;
    bool m_running;
};
